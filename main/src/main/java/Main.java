import annotation.HasGetter;
import processor.ValidatorBuilder;
import processor.ValidatorProcessor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        validateFields();
        System.out.println();

        List<Class<?>> classes = List.of(User.class, Product.class, ValidatorProcessor.class, ValidatorBuilder.class);
        for (Class<?> clazz : classes) {
            try {
                assertGetters(clazz);
                System.out.println(clazz.getName() + ": Assertion succeed");
            } catch (NoSuchMethodException e) {
                System.out.println(clazz.getName() + ": Assertion failed");
                e.printStackTrace();
            }
        }

    }

    public static void validateFields() {
        User user = new User("valid_Login123", "valid.email@gmail.com");
        System.out.println(user);
        System.out.println("Is login valid: " + FieldValidator.loginValidate(user.getLogin()));
        System.out.println("Is email valid: " + FieldValidator.emailValidate(user.getEmail()));

        user = new User("invalid$login.", "daniil.korbut@ukma.edu.ua");
        System.out.println();
        System.out.println(user);
        System.out.println("Is login valid: " + FieldValidator.loginValidate(user.getLogin()));
        System.out.println("Is email valid: " + FieldValidator.emailValidate(user.getEmail()));

        user = new User("anoth3rVal1d", "invalid@ukma.edu.");
        System.out.println();
        System.out.println(user);
        System.out.println("Is login valid: " + FieldValidator.loginValidate(user.getLogin()));
        System.out.println("Is email valid: " + FieldValidator.emailValidate(user.getEmail()));
    }

    public static void assertGetters(Class<?> clazz) throws NoSuchMethodException {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(HasGetter.class)) {
                String fieldName = field.getName();
                String getter = "get" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
                Method getterMethod = clazz.getMethod(getter);
                if (!getterMethod.getReturnType().getName().equals(field.getType().getName())) {
                    throw new NoSuchMethodException(field.getName() + " is " + field.getType().getName()
                            + " but getter returns " + getterMethod.getReturnType().getName());
                }
            }
        }
    }
}
