import annotation.HasGetter;

public class Product {
    @HasGetter
    private String name;
    @HasGetter
    private int price;

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }
}
