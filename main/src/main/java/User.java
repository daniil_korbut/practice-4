import annotation.HasGetter;
import annotation.Validator;

public class User {

    @Validator("[_A-Za-z\\d]+")
    @HasGetter
    private String login;

    @Validator("[.A-Za-z\\d]+@[\\-A-Za-z\\d]+\\.[\\-A-Za-z\\d]+(\\.[\\-A-Za-z\\d]+)*")
    @HasGetter
    private String email;

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public User(String login, String email) {
        this.login = login;
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
