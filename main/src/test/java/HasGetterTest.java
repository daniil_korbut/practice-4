import annotation.HasGetter;
import org.junit.Assert;
import org.junit.Test;
import processor.ValidatorBuilder;
import processor.ValidatorProcessor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

public class HasGetterTest {

    @Test
    public void hasGetterTest() {
        List<Class<?>> classes = List.of(User.class, Product.class, ValidatorProcessor.class, ValidatorBuilder.class);
        for (Class<?> clazz : classes) {
            try {
                assertGetters(clazz);
            } catch (NoSuchMethodException e) {
                Assert.fail(clazz.getName() + ": Assertion failed" + "\n" + e.getMessage());
            }
        }
    }

    private static void assertGetters(Class<?> clazz) throws NoSuchMethodException {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(HasGetter.class)) {
                String fieldName = field.getName();
                String getter = "get" + Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
                Method getterMethod = clazz.getMethod(getter);
                if (!getterMethod.getReturnType().getName().equals(field.getType().getName())) {
                    throw new NoSuchMethodException(field.getName() + " is " + field.getType().getName()
                            + " but getter returns " + getterMethod.getReturnType().getName());
                }
            }
        }
    }

}
