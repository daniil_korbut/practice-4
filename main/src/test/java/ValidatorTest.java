import org.junit.Assert;
import org.junit.Test;

public class ValidatorTest {

    @Test
    public void firstValidatorTest() {
        User user = new User("valid_Login123", "valid.email@gmail.com");
        Assert.assertTrue(FieldValidator.loginValidate(user.getLogin()));
        Assert.assertTrue(FieldValidator.emailValidate(user.getEmail()));
    }

    @Test
    public void secondValidatorTest() {
        User user = new User("invalid$login.", "daniil.korbut@ukma.edu.ua");
        Assert.assertFalse(FieldValidator.loginValidate(user.getLogin()));
        Assert.assertTrue(FieldValidator.emailValidate(user.getEmail()));
    }

    @Test
    public void thirdValidatorTest() {
        User user = new User("anoth3rVal1d", "invalid@ukma.edu.");
        Assert.assertTrue(FieldValidator.loginValidate(user.getLogin()));
        Assert.assertFalse(FieldValidator.emailValidate(user.getEmail()));
    }

}
