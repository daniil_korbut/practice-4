package processor;

import javax.annotation.processing.Filer;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Map;

public class ValidatorBuilder {
    private final Filer filer;
    private final Map<String, String> input;

    public ValidatorBuilder(Filer filer, Map<String, String> input) {
        this.filer = filer;
        this.input = input;
    }

    public void generate() throws IOException {

        JavaFileObject f = filer.createSourceFile("FieldValidator");
        Writer w = f.openWriter();

        try {
            PrintWriter pw = new PrintWriter(w);
            pw.println("public class FieldValidator {");
            for (String fieldName : input.keySet()) {
                pw.println("public static boolean " + fieldName + "Validate(String value) {");
                pw.println("return value.matches(\"" + input.get(fieldName)
                        .replace("\\", "\\\\")
                        .replace("\t", "\\t")
                        .replace("\b", "\\b")
                        .replace("\n", "\\n")
                        .replace("\r", "\\r")
                        .replace("\f", "\\f")
                        .replace("\'", "\\'")
                        .replace("\"", "\\\"") + "\");");
                pw.println("    }");
            }
            pw.println("}");
            pw.flush();
        } finally {
            w.close();
        }


    }
}
