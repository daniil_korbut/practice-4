package processor;

import annotation.Validator;
import com.google.auto.service.AutoService;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.util.*;

@SupportedAnnotationTypes("annotation.Validator")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
@AutoService(Processor.class)
public class ValidatorProcessor extends AbstractProcessor {
    private Filer filer;
    private Messager messager;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        filer = processingEnv.getFiler();
        messager = processingEnv.getMessager();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (annotations.isEmpty()) {
            return false;
        }
        Map<String, String> result = new HashMap<>();
        for (Element annotatedElement : roundEnv.getElementsAnnotatedWith(Validator.class)) {
            if (annotatedElement.getKind() != ElementKind.FIELD) {
                error("Only field can be annotated with Validator", annotatedElement);
                return true;
            }
            /*note("Kind: " + annotatedElement.getKind()
                    + " SimpleName: " + annotatedElement.getSimpleName()
                    + " Class: " + annotatedElement.getClass().toString());*/

//            TypeElement typeElement = (TypeElement) annotatedElement;
//            if (!typeElement.getQualifiedName().toString().equals("java.lang.String")) {
//                error("Only String field can be annotated with Validator", annotatedElement);
//                return true;
//            }
            Validator annotation = annotatedElement.getAnnotation(Validator.class);
            // note(annotatedElement.getSimpleName().toString() + " " +  annotation.value());
            result.putIfAbsent(annotatedElement.getSimpleName().toString(), annotation.value());
        }

        try {
            new ValidatorBuilder(filer, result).generate();
        } catch (IOException e) {
            error(e.getMessage());
        }

        return false;
    }

    private void error(String message, Element element) {
        messager.printMessage(Diagnostic.Kind.ERROR, message, element);
    }

    private void error(String message) {
        messager.printMessage(Diagnostic.Kind.ERROR, message);
    }

    private void note(String message) {
        messager.printMessage(Diagnostic.Kind.NOTE, message);
    }
}
